# OmniDetector

This repository contains a list of image file names of a subset of the [PIROPO][1] training dataset
and annotation data (in Pascal VOC style) for these images.


[1]: https://sites.google.com/site/piropodatabase/
